//
//  TaskCell.swift
//  To-Do List
//
//  Created by Sahil Moradiya on 16/08/21.
//

import UIKit

class TaskCell: UICollectionViewCell {
    @IBOutlet var lblTaskCount: UILabel!
    
    @IBOutlet var progress: UIProgressView!
    @IBOutlet var lblCatName: UILabel!
}
