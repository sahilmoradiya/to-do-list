//
//  LeftSideVC.swift
//  To-Do List
//
//  Created by Sahil Moradiya on 02/08/21.
//

import UIKit
import LGSideMenuController

class LeftSideVC: UIViewController,UITableViewDelegate,UITableViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    //MARK:- IBOutlets
    
    @IBOutlet var txtFirstName: UILabel!
    @IBOutlet var txtLastName: UILabel!
    @IBOutlet var tblView: UITableView!
    @IBOutlet var imgUser: UIImageView!
    
    var arrimg = [#imageLiteral(resourceName: "ic_categorize"),#imageLiteral(resourceName: "ic_chart"),#imageLiteral(resourceName: "ic_settings")]
    var arrname = ["Categories","Analytics","Settings"]
    
    //MARK:- View Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblView.delegate = self
        tblView.dataSource = self
        imgUser.layer.cornerRadius = imgUser.frame.height/2
        view.backgroundColor = UIColor.red
        setUp()
    }
    
    override var prefersStatusBarHidden: Bool
    {
        return true
    }
    
    //MARK:- Methods
    
    func setUp()
    {
        if let imgPic = UserDefaults.standard.imageForKey(key: "userProfile"){
            self.imgUser.image = imgPic
        }
        else{
            self.imgUser.image = UIImage(named: "ic_todo")
        }
        if let fname = UserDefaults.standard.value(forKey: "firstName"){
            self.txtFirstName.text = fname as? String
        }
        else{
            self.txtFirstName.text = "Joy"
        }
        if let lname = UserDefaults.standard.value(forKey: "lastName"){
            self.txtLastName.text = lname as? String
        }
        else{
            self.txtLastName.text = "Mitchell"
        }
    }
    
    //MARK:- IBActions
    
    @IBAction func TapOnBack(_ sender: UIButton) {
        
        self.sideMenuController?.hideLeftView()
    }
    
    @IBAction func TapOnimg(_ sender: UIButton) {
        
    }
    
    //MARK:- TableView Delegate And DataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrimg.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblView.dequeueReusableCell(withIdentifier: "TableViewCell")
            as! TableViewCell
        
        cell.img.image = arrimg[indexPath.row]
        cell.lblmenuname.text = arrname[indexPath.row]
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if indexPath.row == 2
        {
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            let controller = appdelegate.window?.rootViewController as! LGSideMenuController
            let navigation = controller.rootViewController as! UINavigationController
                       
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "SettingVC") as! SettingVC
            navigation.viewControllers = [vc]
            self.sideMenuController?.hideLeftView(animated: true, completion: nil)
        }
        else if indexPath.row == 0
        {
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            let controller = appdelegate.window?.rootViewController as! LGSideMenuController
            let navigation = controller.rootViewController as! UINavigationController
                       
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "CategoriesVC") as! CategoriesVC
            navigation.viewControllers = [vc]
            self.sideMenuController?.hideLeftView(animated: true, completion: nil)
        }
    }
}

    //MARK:- UserDefaults Extension

extension UserDefaults {
    func imageForKey(key: String) -> UIImage? {
        var image: UIImage?
        if let imageData = data(forKey: key) {
            image = NSKeyedUnarchiver.unarchiveObject(with: imageData) as? UIImage
        }
        return image
    }
    func setImage(image: UIImage?, forKey key: String) {
        var imageData: NSData?
        if let image = image {
            imageData = NSKeyedArchiver.archivedData(withRootObject: image) as NSData?
        }
        set(imageData, forKey: key)
    }
}
