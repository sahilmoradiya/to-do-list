//
//  SettingVC.swift
//  To-Do List
//
//  Created by Sahil Moradiya on 20/08/21.
//

import UIKit

class SettingVC: UIViewController, UIImagePickerControllerDelegate & UINavigationControllerDelegate, UITextFieldDelegate {

    //MARK:- IBOutlet
    
    @IBOutlet var txtLastName: UITextField!
    @IBOutlet var imgUser: UIImageView!
    @IBOutlet var txtFirstName: UITextField!
    @IBOutlet var btnBack: UIButton!
    
    let appDelegate = UIApplication.shared.delegate as? AppDelegate
    
    //MARK:- View Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        imgUser.layer.cornerRadius = imgUser.frame.height/2
        setUp()
        txtLastName.delegate = self
        txtFirstName.delegate = self
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    //MARK:- Methods
    
    func setUp()
    {
        if let imgPic = UserDefaults.standard.imageForKey(key: "userProfile"){
            self.imgUser.image = imgPic
        }
        else{
            self.imgUser.image = UIImage(named: "ic_todo")
        }
        if let fname = UserDefaults.standard.value(forKey: "firstName"){
            self.txtFirstName.text = fname as? String
        }
        else{
            self.txtFirstName.text = "Joy"
        }
        if let lname = UserDefaults.standard.value(forKey: "lastName"){
            self.txtLastName.text = lname as? String
        }
        else{
            self.txtLastName.text = "Mitchell"
        }
    }
    
    func validation() -> Bool
    {
        if txtFirstName.text == "" {
            
            let alert = UIAlertController.init(title: "Please Enter First Name", message: "", preferredStyle: .alert)
            let alertaction = UIAlertAction.init(title: "OK", style: .default, handler: nil)
            alert.addAction(alertaction)
            self.present(alert, animated: true, completion: nil)
            return false
        }
        else if txtLastName.text == ""
        {
            let alert = UIAlertController.init(title: "Please Enter Last Name", message: "", preferredStyle: .alert)
            let alertaction = UIAlertAction.init(title: "OK", style: .default, handler: nil)
            alert.addAction(alertaction)
            self.present(alert, animated: true, completion: nil)
            return false
        }
        return true
    }
    
    //MARK:- TextField Delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case txtFirstName:
            txtLastName.becomeFirstResponder()
        case txtLastName:
            txtLastName.resignFirstResponder()
        default:
            txtLastName.resignFirstResponder()
        }
        return true
    }
    
    //MARK:- IBActions

    @IBAction func TapOnBack(_ sender: Any) {
        
        appDelegate?.setUpSideMenu()
    }
    
    @IBAction func TapOnChangImage(_ sender: Any) {
        
        let imgpicker = UIImagePickerController()
        imgpicker.delegate = self
        imgpicker.allowsEditing = true
        let alert = UIAlertController(title: "Select Image", message: "", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            imgpicker.sourceType = .camera
            self.present(imgpicker, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { (action) in
            imgpicker.sourceType = .photoLibrary
            self.present(imgpicker, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    //MARK:- ImagePicket Delegate
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage
        imgUser.image = image
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func TapOnSave(_ sender: Any) {
        
        if validation()
        {
            UserDefaults.standard.setValue(txtFirstName.text ?? "", forKey: "firstName")
            UserDefaults.standard.setValue(txtLastName.text ?? "", forKey: "lastName")
            UserDefaults.standard.setImage(image: imgUser.image, forKey: "userProfile")
            
            appDelegate?.setUpSideMenu()
        }
    }
}
