//
//  NewTaskVC.swift
//  To-Do List
//
//  Created by Sahil Moradiya on 18/08/21.
//

import UIKit

class NewTaskVC: UIViewController, UITextFieldDelegate {

    //MARK:- IBOutlets
    
    @IBOutlet var btnPersonal: UIButton!
    @IBOutlet var btnBusiness: UIButton!
    @IBOutlet var btnDone: UIButton!
    @IBOutlet var txtTaskName: UITextField!
    
    var delegate: NewTask?
    
    var isType = "Business"
    
    //MARK:- View Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        txtTaskName.delegate = self
        txtTaskName.becomeFirstResponder()
    }

    //MARK:- TextField Delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case txtTaskName:
            txtTaskName.resignFirstResponder()
        default:
            txtTaskName.resignFirstResponder()
        }
        return true
    }
    
    //MARK:- IBActions
    
    @IBAction func TapOnCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
        
    @IBAction func TapOnBusiness(_ sender: Any) {
        
        btnBusiness.setTitleColor(UIColor.white, for: .normal)
        btnBusiness.backgroundColor = UIColor.darkGray
        
        btnPersonal.setTitleColor(UIColor.darkGray, for: .normal)
        btnPersonal.backgroundColor = UIColor.white
        
        isType = "Business"
        
    }
    
    
    @IBAction func TapOnPersonal(_ sender: Any) {
        
        btnBusiness.setTitleColor(UIColor.darkGray, for: .normal)
        btnBusiness.backgroundColor = UIColor.white
        
        btnPersonal.setTitleColor(UIColor.white, for: .normal)
        btnPersonal.backgroundColor = UIColor.darkGray
        
        isType = "Personal"

    }
    
    //MARK:- Methods
    
    func checkTask() -> Bool
    {
        if txtTaskName.text == "" {
            
            let alert = UIAlertController.init(title: "Please Enter New Task", message: "", preferredStyle: .alert)
            let alertaction = UIAlertAction.init(title: "OK", style: .default, handler: nil)
            alert.addAction(alertaction)
            self.present(alert, animated: true, completion: nil)
            return false
        }
        return true
    }
    
    @IBAction func TapOnDone(_ sender: Any) {
        
        if checkTask()
        {
            self.delegate?.newTask(Task: txtTaskName.text ?? "", Type: isType)
            self.dismiss(animated: true, completion: nil)
        }
        else
        {
            print("New Task Empty")
        }
    }
}
