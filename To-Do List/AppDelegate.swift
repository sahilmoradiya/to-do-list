//
//  AppDelegate.swift
//  To-Do List
//
//  Created by Sahil Moradiya on 02/08/21.
//

import UIKit
import LGSideMenuController



@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    var arrTask = NSMutableArray()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let vc = storyboard.instantiateViewController(withIdentifier: "RootVC") as! RootVC
//        let navigation = UINavigationController(rootViewController: vc)
//        navigation.navigationBar.isTranslucent = true
//        self.window?.rootViewController = navigation
        
        setUpSideMenu()

        return true
    }

    
    func setUpSideMenu(){
          let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
          let home = mainStoryboard.instantiateViewController(withIdentifier: "RootVC") as! RootVC
          let homeNavigation = UINavigationController(rootViewController: home)
          let sideMenuVC = mainStoryboard.instantiateViewController(withIdentifier: "LeftSideVC") as! LeftSideVC
          let controller = LGSideMenuController.init(rootViewController: homeNavigation, leftViewController: sideMenuVC, rightViewController: nil)
          controller.navigationController?.navigationBar.isHidden = true
          controller.leftViewWidth = UIScreen.main.bounds.width - 80
          controller.leftViewPresentationStyle = .slideBelow
          self.window?.rootViewController = controller
          self.window?.makeKeyAndVisible()
      }
}

