//
//  TableTaskCell.swift
//  To-Do List
//
//  Created by Sahil Moradiya on 16/08/21.
//

import UIKit

class TableTaskCell: UITableViewCell {

    @IBOutlet var lblTask: UILabel!
    @IBOutlet var lblType: UILabel!
    @IBOutlet var btnCheck: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
