//
//  TableViewCell.swift
//  To-Do List
//
//  Created by Sahil Moradiya on 02/08/21.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet var lblmenuname: UILabel!
    @IBOutlet var img: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
