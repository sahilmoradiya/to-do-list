//
//  CategoriesVC.swift
//  To-Do List
//
//  Created by Sahil Moradiya on 21/08/21.
//

import UIKit

class CategoriesVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //MARK:- IBOutlets
    
    @IBOutlet var btnBack: UIButton!
    @IBOutlet var tblView: UITableView!
    
    @IBOutlet var imgEmpty: UIImageView!
    var totalP:Int = Int()
    var totalB:Int = Int()
    
    var arrP = NSMutableArray()
    var arrB = NSMutableArray()
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    //MARK:- View Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tblView.delegate = self
        tblView.dataSource = self
        
        Counting()
    }
    
    //MARK:- Methods
    
    func Counting()
    {
        for obj in appDelegate.arrTask
        {
            let dicObj = obj as? NSMutableDictionary
            let type = dicObj?.value(forKey: "type") as? String
            
            if type == "Personal"
            {
                arrP.add(dicObj!)
            }
            else
            {
                arrB.add(dicObj!)
            }
        }
        if arrB.count == 0 && arrP.count == 0
        {
            imgEmpty.isHidden = false
        }
        else
        {
            imgEmpty.isHidden = true
        }

    }
    
    //MARK:- IBActions

    @IBAction func TapOnBack(_ sender: Any) {
        
        appDelegate.setUpSideMenu()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0
        {
            return arrB.count
        }
        else
        {
            return arrP.count
        }
    }
    
    //MARK: - TableView Delegate And DatSources
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0
        {
            let dicData = arrB[indexPath.row] as? NSMutableDictionary
            let cell = tblView.dequeueReusableCell(withIdentifier: "CateCell") as! CateCell
            cell.lblTask.text = dicData?.value(forKey: "name") as? String
            return cell
        }
        else
        {
            let dicData = arrP[indexPath.row] as? NSMutableDictionary
            let cell = tblView.dequeueReusableCell(withIdentifier: "CateCell") as! CateCell
            cell.lblTask.text = dicData?.value(forKey: "name") as? String
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var headerView = UIView()
        if section == 0
        {
            let arrView = Bundle.main.loadNibNamed("Business", owner: self, options: nil)
            headerView = (arrView?.first as? Business)!
        }
        else
        {
            let arrView = Bundle.main.loadNibNamed("Personal", owner: self, options: nil)
            headerView = (arrView?.first as? Personal)!
        }
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if section == 0
        {
            if arrB.count == 0
            {
                return 0.0
            }
        }
        else if section == 1
        {
            if arrP.count == 0
            {
                return 0.0
            }
        }
        return 35
    }
}
