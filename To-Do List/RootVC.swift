//
//  RootVC.swift
//  To-Do List
//
//  Created by Sahil Moradiya on 02/08/21.
//

import UIKit
import LGSideMenuController

//MARK:- Protocol

protocol NewTask {
    func newTask(Task: String, Type: String)
}

class RootVC: UIViewController, NewTask {
    
    //MARK:- IBOutlet
    
    @IBOutlet var lblWhat: UILabel!
    @IBOutlet var imgLogo: UIImageView!
    @IBOutlet var collView: UICollectionView!
    @IBOutlet var tblView: UITableView!
    var arrCatName = ["Business","Personal"]
    
    var business:Int = 0
    var personal:Int = 0
    
    var arrB = NSMutableArray()
    var arrP = NSMutableArray()
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate

    var flowLayoutAuto: UICollectionViewFlowLayout {
        let _flowLayout = UICollectionViewFlowLayout()
        
        DispatchQueue.main.async {
            _flowLayout.itemSize = CGSize(width: 250, height: 150)
        }
        
        _flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        _flowLayout.minimumLineSpacing = 20.0
        _flowLayout.scrollDirection = UICollectionView.ScrollDirection.horizontal
        
        return _flowLayout
    }

    //MARK:- View Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isHidden = true
        collView.delegate = self
        collView.dataSource = self
        collView.collectionViewLayout = flowLayoutAuto
        
        appDelegate.arrTask = getCurrentUserData()
        
        CountingType()
        
        collView.reloadData()

        tblView.delegate = self
        tblView.dataSource = self
        setUp()
 
    }
    
    //MARK:- Methods
    
    func setUp()
    {
        if let fname = UserDefaults.standard.value(forKey: "firstName"){
            self.lblWhat.text = "What's up, \(fname)!"
        }
        else{
            self.lblWhat.text = "What's up, Joy!"
        }
    }
    
    func newTask(Task: String, Type: String) {
        
        let dic = NSMutableDictionary()
        dic.setValue(Task, forKey: "name")
        dic.setValue(Type, forKey: "type")
        dic.setValue(false, forKey: "isSelected")
        
        appDelegate.arrTask.add(dic)
        
        arrP.removeAllObjects()
        arrB.removeAllObjects()

        CountingType()
   
        saveCurrentUserData(arr: appDelegate.arrTask)
        collView.reloadData()
        tblView.reloadData()
        
    }
    
    func CountingType()
    {
        for obj in appDelegate.arrTask
        {
            let dicObj = obj as? NSMutableDictionary
            let type = dicObj?.value(forKey: "type") as? String
            
            if type == "Personal"{
                arrP.add(dicObj!)
            }
            else{
                arrB.add(dicObj!)
            }
        }
        if appDelegate.arrTask.count == 0{
            imgLogo.isHidden = false
        }
        else{
            imgLogo.isHidden = true
        }
    }
    
    //MARK:- IBActions
   
    @IBAction func clickedMenu(_ sender: Any) {
        self.sideMenuController?.showLeftView()
    }
    
    @IBAction func TapOnAdd(_ sender: Any) {
        let taskVC = storyboard?.instantiateViewController(withIdentifier: "NewTaskVC") as! NewTaskVC
        taskVC.delegate = self
        taskVC.modalPresentationStyle = .fullScreen
        self.navigationController?.navigationBar.isHidden = true
        self.present(taskVC, animated: true, completion: nil)
    }
    
    //MARK:- Save Data
    
    func saveCurrentUserData(arr: NSMutableArray)
    {
        let data = NSKeyedArchiver.archivedData(withRootObject: arr)
        UserDefaults.standard.setValue(data, forKey: "CURRENT_USER_DATA")
        UserDefaults.standard.synchronize()
    }
    
    func getCurrentUserData() -> NSMutableArray
    {
        if let data = UserDefaults.standard.object(forKey: "CURRENT_USER_DATA"){
            
            let arrayObjc = NSKeyedUnarchiver.unarchiveObject(with: data as! Data)
            return arrayObjc as! NSMutableArray
        }
        return NSMutableArray()
    }
}

    //MARK:- CollectionView Delegate And DataSource

extension RootVC: UICollectionViewDelegate, UICollectionViewDataSource
{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrCatName.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collView.dequeueReusableCell(withReuseIdentifier: "TaskCell", for: indexPath) as! TaskCell
        cell.lblCatName.text = arrCatName[indexPath.row]
        if indexPath.row == 0
        {
            cell.lblTaskCount.text = "\(arrB.count) tasks"
        }
        if indexPath.row == 1
        {
            cell.progress.tintColor = UIColor.blue
            cell.lblTaskCount.text = "\(arrP.count) tasks"
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        let controller = appdelegate.window?.rootViewController as! LGSideMenuController
        let navigation = controller.rootViewController as! UINavigationController
                   
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "CategoriesVC") as! CategoriesVC
        navigation.viewControllers = [vc]
        self.sideMenuController?.hideLeftView(animated: true, completion: nil)
    }
}

    //MARK:- TableView Delegate And DataSource


extension RootVC: UITableViewDelegate, UITableViewDataSource
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return appDelegate.arrTask.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblView.dequeueReusableCell(withIdentifier: "TableTaskCell") as! TableTaskCell
        
        let dicData = appDelegate.arrTask[indexPath.row] as? NSMutableDictionary
        
        
        cell.lblTask.text = dicData?.value(forKey: "name") as? String
        cell.lblType.text = dicData?.value(forKey: "type") as? String
        
        let isSelected = dicData?.value(forKey: "isSelected") as? Bool
        
        if isSelected == true
        {
            cell.btnCheck.setImage(UIImage(named: "ic_check"), for: .normal)

        }
        else
        {
            cell.btnCheck.setImage(UIImage(named: "ic_uncheck"), for: .normal)

        }
        
        cell.btnCheck.tag = indexPath.row
        cell.btnCheck.addTarget(self, action: #selector(tappedButton(sender:)), for: .touchUpInside)

        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == UITableViewCell.EditingStyle.delete
        {
            appDelegate.arrTask.removeObject(at: indexPath.row)
            tblView.deleteRows(at: [indexPath], with: .top)
            
            arrP.removeAllObjects()
            arrB.removeAllObjects()

            CountingType()
            
            saveCurrentUserData(arr: appDelegate.arrTask)
            collView.reloadData()
            tblView.reloadData()
        }
    }
    
    @objc func tappedButton(sender: UIButton)
    {

        let dicData = appDelegate.arrTask[sender.tag] as? NSMutableDictionary

        let cellNew = sender.superview?.superview?.superview as! TableTaskCell
        
        if cellNew.btnCheck.currentImage == UIImage(named: "ic_check")
        {
            dicData?.setValue(false, forKey: "isSelected")
            cellNew.btnCheck.setImage(UIImage(named: "ic_uncheck"), for: .normal)
        }
        else
        {
            dicData?.setValue(true, forKey: "isSelected")
            cellNew.btnCheck.setImage(UIImage(named: "ic_check"), for: .normal)
        }

        DispatchQueue.main.async {
            self.saveCurrentUserData(arr: self.appDelegate.arrTask)
        }
    }
    
}
